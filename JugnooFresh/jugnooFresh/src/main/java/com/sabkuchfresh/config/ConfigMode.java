package com.sabkuchfresh.config;

/**
 * Created by shankar on 4/29/15.
 */
public enum ConfigMode {
    DEV, LIVE, DEV_1, DEV_2, DEV_3, CUSTOM
}
