package com.sabkuchfresh.home;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sabkuchfresh.AccountActivity;
import com.sabkuchfresh.Constants;
import com.sabkuchfresh.Data;
import com.sabkuchfresh.R;
import com.sabkuchfresh.datastructure.AddPaymentPath;
import com.sabkuchfresh.fresh.FreshActivity;
import com.sabkuchfresh.utils.FlurryEventLogger;
import com.sabkuchfresh.utils.FlurryEventNames;
import com.sabkuchfresh.utils.Fonts;
import com.sabkuchfresh.utils.NudgeClient;
import com.sabkuchfresh.utils.ProgressWheel;
import com.sabkuchfresh.utils.Utils;
import com.sabkuchfresh.wallet.PaymentActivity;
import com.squareup.picasso.CircleTransform;
import com.squareup.picasso.Picasso;

/**
 * Created by shankar on 4/8/16.
 */
public class MenuBar {

	Activity activity;
	DrawerLayout drawerLayout;

	//menu bar 
	public LinearLayout menuLayout;

	public LinearLayout linearLayoutProfile;
	public ImageView imageViewProfile;
	public TextView textViewUserName, textViewViewAccount;

	public RelativeLayout relativeLayoutGamePredict;
	public ImageView imageViewGamePredict;
	public TextView textViewGamePredict, textViewGamePredictNew;

	public RelativeLayout relativeLayoutGetRide;
	public ImageView imageViewGetRide, imageViewFresh;
	public TextView textViewGetRide;

	public RelativeLayout relativeLayoutInvite;
	public TextView textViewInvite;

	public RelativeLayout relativeLayoutWallet;
	public TextView textViewWallet, textViewWalletValue;
	public ProgressWheel progressBarMenuPaytmWalletLoading;

	public RelativeLayout relativeLayoutPromotions;
	public TextView textViewPromotions, textViewPromotionsValue;

	public RelativeLayout relativeLayoutTransactions;
	public TextView textViewTransactions;
	public ImageView imageViewTransactions;

	public RelativeLayout relativeLayoutReferDriver;
	public TextView textViewReferDriver, textViewReferDriverNew;

	public RelativeLayout relativeLayoutSupport;
	public TextView textViewSupport;

	public RelativeLayout relativeLayoutAbout;
	public TextView textViewAbout;

	public MenuBar(Activity activity, DrawerLayout rootView){
		this.activity = activity;
		this.drawerLayout = rootView;
		initComponents();
	}


	private void initComponents(){
		//Swipe menu
		menuLayout = (LinearLayout) drawerLayout.findViewById(R.id.menuLayout);


		linearLayoutProfile = (LinearLayout) drawerLayout.findViewById(R.id.linearLayoutProfile);
		imageViewProfile = (ImageView) drawerLayout.findViewById(R.id.imageViewProfile);
		textViewUserName = (TextView) drawerLayout.findViewById(R.id.textViewUserName);
		textViewUserName.setTypeface(Fonts.mavenRegular(activity));
		textViewViewAccount = (TextView) drawerLayout.findViewById(R.id.textViewViewAccount);
		textViewViewAccount.setTypeface(Fonts.latoRegular(activity));

		relativeLayoutGamePredict = (RelativeLayout) drawerLayout.findViewById(R.id.relativeLayoutGamePredict);
		imageViewGamePredict = (ImageView) drawerLayout.findViewById(R.id.imageViewGamePredict);
		textViewGamePredict = (TextView)drawerLayout.findViewById(R.id.textViewGamePredict); textViewGamePredict.setTypeface(Fonts.mavenLight(activity));
		textViewGamePredictNew = (TextView)drawerLayout.findViewById(R.id.textViewGamePredictNew); textViewGamePredictNew.setTypeface(Fonts.mavenLight(activity));

		relativeLayoutGetRide = (RelativeLayout) drawerLayout.findViewById(R.id.relativeLayoutGetRide);
		textViewGetRide = (TextView) drawerLayout.findViewById(R.id.textViewGetRide);
		textViewGetRide.setTypeface(Fonts.mavenLight(activity));
		imageViewGetRide = (ImageView) drawerLayout.findViewById(R.id.imageViewGetRide);
		imageViewFresh = (ImageView) drawerLayout.findViewById(R.id.imageViewFresh);

		relativeLayoutInvite = (RelativeLayout) drawerLayout.findViewById(R.id.relativeLayoutInvite);
		textViewInvite = (TextView) drawerLayout.findViewById(R.id.textViewInvite);
		textViewInvite.setTypeface(Fonts.mavenLight(activity));

		relativeLayoutWallet = (RelativeLayout) drawerLayout.findViewById(R.id.relativeLayoutWallet);
		textViewWallet = (TextView) drawerLayout.findViewById(R.id.textViewWallet);
		textViewWallet.setTypeface(Fonts.mavenLight(activity));
		textViewWalletValue = (TextView) drawerLayout.findViewById(R.id.textViewWalletValue);
		textViewWalletValue.setTypeface(Fonts.latoRegular(activity));
		progressBarMenuPaytmWalletLoading = (ProgressWheel) drawerLayout.findViewById(R.id.progressBarMenuPaytmWalletLoading);
		progressBarMenuPaytmWalletLoading.setVisibility(View.GONE);

		relativeLayoutPromotions = (RelativeLayout) drawerLayout.findViewById(R.id.relativeLayoutPromotions);
		textViewPromotions = (TextView) drawerLayout.findViewById(R.id.textViewPromotions);
		textViewPromotions.setTypeface(Fonts.mavenLight(activity));
		textViewPromotionsValue = (TextView) drawerLayout.findViewById(R.id.textViewPromotionsValue);
		textViewPromotionsValue.setTypeface(Fonts.latoRegular(activity));
		textViewPromotionsValue.setVisibility(View.GONE);

		relativeLayoutTransactions = (RelativeLayout) drawerLayout.findViewById(R.id.relativeLayoutTransactions);
		textViewTransactions = (TextView) drawerLayout.findViewById(R.id.textViewTransactions);
		textViewTransactions.setTypeface(Fonts.mavenLight(activity));
		imageViewTransactions = (ImageView) drawerLayout.findViewById(R.id.imageViewTransactions);

		relativeLayoutReferDriver = (RelativeLayout) drawerLayout.findViewById(R.id.relativeLayoutReferDriver);
		textViewReferDriver = (TextView) drawerLayout.findViewById(R.id.textViewReferDriver);
		textViewReferDriver.setTypeface(Fonts.mavenLight(activity));
		textViewReferDriverNew = (TextView) drawerLayout.findViewById(R.id.textViewReferDriverNew);
		textViewReferDriverNew.setTypeface(Fonts.mavenLight(activity));

		relativeLayoutSupport = (RelativeLayout) drawerLayout.findViewById(R.id.relativeLayoutSupport);
		textViewSupport = (TextView) drawerLayout.findViewById(R.id.textViewSupport);
		textViewSupport.setTypeface(Fonts.mavenLight(activity));

		relativeLayoutAbout = (RelativeLayout) drawerLayout.findViewById(R.id.relativeLayoutAbout);
		textViewAbout = (TextView) drawerLayout.findViewById(R.id.textViewAbout);
		textViewAbout.setTypeface(Fonts.mavenLight(activity));




		// menu events
		linearLayoutProfile.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				activity.startActivity(new Intent(activity, AccountActivity.class));
				activity.overridePendingTransition(R.anim.right_in, R.anim.right_out);
				FlurryEventLogger.event(activity, FlurryEventNames.CLICKS_ON_ACCOUNT);
			}
		});

		relativeLayoutGetRide.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(activity instanceof FreshActivity){
					activity.finish();
					activity.overridePendingTransition(R.anim.grow_from_middle, R.anim.shrink_to_middle);
				}
			}
		});

		relativeLayoutWallet.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(activity, PaymentActivity.class);
				intent.putExtra(Constants.KEY_ADD_PAYMENT_PATH, AddPaymentPath.WALLET.getOrdinal());
				activity.startActivity(intent);
				activity.overridePendingTransition(R.anim.right_in, R.anim.right_out);
				FlurryEventLogger.event(FlurryEventNames.WALLET_MENU);
				FlurryEventLogger.event(activity, FlurryEventNames.CLICKS_ON_WALLET);
				NudgeClient.trackEventUserId(activity, FlurryEventNames.NUDGE_WALLET_CLICKED, null);
			}
		});

		relativeLayoutTransactions.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(activity instanceof FreshActivity){
					((FreshActivity)activity).openOrderHistory();
					//drawerLayout.closeDrawer(menuLayout);
				}
			}
		});

		relativeLayoutSupport.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(activity instanceof FreshActivity){
					((FreshActivity)activity).openSupport();
					//drawerLayout.closeDrawer(menuLayout);
				}
			}
		});



		menuLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

			}
		});


		try {
			if(Data.userData.getGamePredictEnable() == 1
					&& !"".equalsIgnoreCase(Data.userData.getGamePredictName())){
				relativeLayoutGamePredict.setVisibility(View.VISIBLE);
				textViewGamePredict.setText(Data.userData.getGamePredictName());
				if(!"".equalsIgnoreCase(Data.userData.getGamePredictNew())){
					textViewGamePredictNew.setText(Data.userData.getGamePredictNew());
				} else{
					textViewGamePredictNew.setVisibility(View.GONE);
				}
				try {
					if(!"".equalsIgnoreCase(Data.userData.getGamePredictIconUrl())){
						Picasso.with(activity)
								.load(Data.userData.getGamePredictIconUrl())
								.placeholder(R.drawable.ic_worldcup)
								.error(R.drawable.ic_worldcup)
								.into(imageViewGamePredict);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else{
				relativeLayoutGamePredict.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(activity instanceof FreshActivity){
			textViewGetRide.setText(activity.getResources().getString(R.string.get_a_ride));
			imageViewGetRide.setVisibility(View.VISIBLE);
			imageViewFresh.setVisibility(View.GONE);
			relativeLayoutInvite.setVisibility(View.GONE);
			relativeLayoutPromotions.setVisibility(View.GONE);
			relativeLayoutAbout.setVisibility(View.GONE);
			textViewTransactions.setText(activity.getResources().getString(R.string.order_history));
			relativeLayoutReferDriver.setVisibility(View.GONE);
		}

	}

	public void setUserData(){
		try {
			textViewUserName.setText(Data.userData.userName);

			if(Data.userData.numCouponsAvaliable > 0) {
				textViewPromotionsValue.setVisibility(View.VISIBLE);
				textViewPromotionsValue.setText("" + Data.userData.numCouponsAvaliable);
			}
			else{
				textViewPromotionsValue.setVisibility(View.GONE);
			}

			textViewWalletValue.setText(String.format(activity.getResources().getString(R.string.rupees_value_format),
					Utils.getMoneyDecimalFormat().format(Data.userData.getTotalWalletBalance())));

			Data.userData.userImage = Data.userData.userImage.replace("http://graph.facebook", "https://graph.facebook");
			try {
				Picasso.with(activity).load(Data.userData.userImage).skipMemoryCache().transform(new CircleTransform()).into(imageViewProfile);
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void dismissPaytmLoading(){
		try {
			progressBarMenuPaytmWalletLoading.setVisibility(View.GONE);
			textViewWalletValue.setVisibility(View.VISIBLE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
