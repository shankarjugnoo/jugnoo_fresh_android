package com.sabkuchfresh.home;

import android.app.Activity;
import android.graphics.drawable.StateListDrawable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sabkuchfresh.Data;
import com.sabkuchfresh.R;
import com.sabkuchfresh.config.Config;
import com.sabkuchfresh.datastructure.SPLabels;
import com.sabkuchfresh.fresh.FreshActivity;
import com.sabkuchfresh.utils.FlurryEventLogger;
import com.sabkuchfresh.utils.FlurryEventNames;
import com.sabkuchfresh.utils.Fonts;
import com.sabkuchfresh.utils.NudgeClient;
import com.sabkuchfresh.utils.Prefs;

/**
 * Created by shankar on 4/8/16.
 */
public class TopBar {


	Activity activity;
	DrawerLayout drawerLayout;

	//Top RL
	public RelativeLayout topRl;
	public ImageView imageViewMenu, imageViewSearchCancel;
	public TextView title;
	public Button buttonCheckServer;
	public ImageView imageViewHelp;
	public RelativeLayout relativeLayoutNotification;
	public TextView textViewNotificationValue;
	public ImageView imageViewBack, imageViewDelete;
	public TextView textViewAdd;

	public LinearLayout linearLayoutFreshSwapper;
	public ImageView imageViewAutoSwapper, imageViewFreshSwapper;

	public TopBar(Activity activity, DrawerLayout drawerLayout){
		this.activity = activity;
		this.drawerLayout = drawerLayout;
		setupTopBar();
	}

	private void setupTopBar(){
		topRl = (RelativeLayout) drawerLayout.findViewById(R.id.topRl);
		imageViewMenu = (ImageView) drawerLayout.findViewById(R.id.imageViewMenu);
		imageViewSearchCancel = (ImageView) drawerLayout.findViewById(R.id.imageViewSearchCancel);
		title = (TextView) drawerLayout.findViewById(R.id.title);title.setTypeface(Fonts.mavenRegular(activity));
		buttonCheckServer = (Button) drawerLayout.findViewById(R.id.buttonCheckServer);
		imageViewHelp = (ImageView) drawerLayout.findViewById(R.id.imageViewHelp);
		relativeLayoutNotification = (RelativeLayout) drawerLayout.findViewById(R.id.relativeLayoutNotification);
		textViewNotificationValue = (TextView) drawerLayout.findViewById(R.id.textViewNotificationValue);
		textViewNotificationValue.setTypeface(Fonts.latoRegular(activity));
		textViewNotificationValue.setVisibility(View.GONE);

		imageViewBack = (ImageView) drawerLayout.findViewById(R.id.imageViewBack);
		imageViewDelete = (ImageView) drawerLayout.findViewById(R.id.imageViewDelete);
		textViewAdd = (TextView) drawerLayout.findViewById(R.id.textViewAdd); textViewAdd.setTypeface(Fonts.mavenRegular(activity));
		linearLayoutFreshSwapper = (LinearLayout) drawerLayout.findViewById(R.id.linearLayoutFreshSwapper);
		imageViewAutoSwapper = (ImageView) drawerLayout.findViewById(R.id.imageViewAutoSwapper);
		imageViewFreshSwapper = (ImageView) drawerLayout.findViewById(R.id.imageViewFreshSwapper);


		//Top bar events
		topRl.setOnClickListener(topBarOnClickListener);
		imageViewMenu.setOnClickListener(topBarOnClickListener);
		buttonCheckServer.setOnClickListener(topBarOnClickListener);

		buttonCheckServer.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				Toast.makeText(activity, Config.getServerUrlName(), Toast.LENGTH_SHORT).show();
				FlurryEventLogger.checkServerPressed(Data.userData.accessToken);
				return false;
			}
		});

		imageViewSearchCancel.setOnClickListener(topBarOnClickListener);
		imageViewHelp.setOnClickListener(topBarOnClickListener);
		relativeLayoutNotification.setOnClickListener(topBarOnClickListener);
		imageViewBack.setOnClickListener(topBarOnClickListener);
		imageViewDelete.setOnClickListener(topBarOnClickListener);
		textViewAdd.setOnClickListener(topBarOnClickListener);
		imageViewAutoSwapper.setOnClickListener(topBarOnClickListener);
		imageViewFreshSwapper.setOnClickListener(topBarOnClickListener);

		if(activity instanceof FreshActivity){
			relativeLayoutNotification.setVisibility(View.VISIBLE);
			imageViewHelp.setVisibility(View.GONE);
			imageViewSearchCancel.setVisibility(View.GONE);
			title.setText(activity.getResources().getString(R.string.jugnoo_fresh));

			title.setVisibility(View.GONE);
			linearLayoutFreshSwapper.setVisibility(View.VISIBLE);
			imageViewAutoSwapper.setImageDrawable(getStateListDrawable(R.drawable.ic_swap_auto_alpha, R.drawable.ic_swap_auto));
			imageViewFreshSwapper.setImageResource(R.drawable.ic_swap_fresh);

		}

		setupFreshUI();

	}

	private View.OnClickListener topBarOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			switch(v.getId()){
				case R.id.topRl:
					break;

				case R.id.imageViewMenu:
					drawerLayout.openDrawer(GravityCompat.START);
					FlurryEventLogger.event(FlurryEventNames.MENU_LOOKUP);
					NudgeClient.trackEventUserId(activity, FlurryEventNames.NUDGE_MENU_CLICKED, null);
					break;

				case R.id.buttonCheckServer:
					break;

				case R.id.imageViewBack:
					if(activity instanceof FreshActivity){
						((FreshActivity)activity).performBackPressed();
					}
					break;

				case R.id.imageViewDelete:
					if(activity instanceof FreshActivity){
						((FreshActivity)activity).deleteCart();
					}
					break;

				case R.id.textViewAdd:
					if(activity instanceof FreshActivity){
						((FreshActivity)activity).addAddress();
					}
					break;

				case R.id.imageViewAutoSwapper:
					if(activity instanceof FreshActivity){
						activity.finish();
						activity.overridePendingTransition(R.anim.grow_from_middle, R.anim.shrink_to_middle);
						NudgeClient.trackEventUserId(activity, FlurryEventNames.NUDGE_FRESH_BACK_TO_JUGNOO, null);
					}
					break;

			}
		}
	};



	public void setUserData(){
		try {
			int unreadNotificationsCount = Prefs.with(activity).getInt(SPLabels.NOTIFICATION_UNREAD_COUNT, 0);
			if(unreadNotificationsCount > 0){
				textViewNotificationValue.setVisibility(View.VISIBLE);
				textViewNotificationValue.setText(String.valueOf(unreadNotificationsCount));
			}
			else{
				textViewNotificationValue.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private StateListDrawable getStateListDrawable(int resourceNormal, int resourcePressed){
		StateListDrawable stateListDrawable = new StateListDrawable();
		stateListDrawable.addState(new int[]{android.R.attr.state_pressed},
				activity.getResources().getDrawable(resourcePressed));
		stateListDrawable.addState(new int[]{},
				activity.getResources().getDrawable(resourceNormal));
		return stateListDrawable;
	}

	public void setupFreshUI(){
		try {
			title.setVisibility(View.GONE);
			linearLayoutFreshSwapper.setVisibility(View.VISIBLE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void closeFreshUI(){
		title.setVisibility(View.VISIBLE);
		linearLayoutFreshSwapper.setVisibility(View.GONE);
	}

}
