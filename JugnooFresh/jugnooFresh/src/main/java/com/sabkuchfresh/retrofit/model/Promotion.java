package com.sabkuchfresh.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 3/19/16.
 */
public class Promotion {

	@SerializedName("promo_id")
	@Expose
	private Integer promoId;
	@SerializedName("title")
	@Expose
	private String title;
	@SerializedName("max_allowed")
	@Expose
	private Integer maxAllowed;
	@SerializedName("terms_n_conds")
	@Expose
	private String termsNConds;

	/**
	 *
	 * @return
	 * The promoId
	 */
	public Integer getPromoId() {
		return promoId;
	}

	/**
	 *
	 * @param promoId
	 * The promo_id
	 */
	public void setPromoId(Integer promoId) {
		this.promoId = promoId;
	}

	/**
	 *
	 * @return
	 * The title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 *
	 * @param title
	 * The title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 *
	 * @return
	 * The maxAllowed
	 */
	public Integer getMaxAllowed() {
		return maxAllowed;
	}

	/**
	 *
	 * @param maxAllowed
	 * The max_allowed
	 */
	public void setMaxAllowed(Integer maxAllowed) {
		this.maxAllowed = maxAllowed;
	}

	/**
	 *
	 * @return
	 * The termsNConds
	 */
	public String getTermsNConds() {
		return termsNConds;
	}

	/**
	 *
	 * @param termsNConds
	 * The terms_n_conds
	 */
	public void setTermsNConds(String termsNConds) {
		this.termsNConds = termsNConds;
	}

}