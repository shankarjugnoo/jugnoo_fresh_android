package com.sabkuchfresh.utils;

public interface IDeviceTokenReceiver {
	public void deviceTokenReceived(String regId);
}
