package com.sabkuchfresh.fresh.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.flurry.android.FlurryAgent;
import com.sabkuchfresh.Constants;
import com.sabkuchfresh.Data;
import com.sabkuchfresh.JSONParser;
import com.sabkuchfresh.R;
import com.sabkuchfresh.SplashNewActivity;
import com.sabkuchfresh.config.Config;
import com.sabkuchfresh.datastructure.DialogErrorType;
import com.sabkuchfresh.fresh.FreshActivity;
import com.sabkuchfresh.fresh.FreshNoDeliveriesDialog;
import com.sabkuchfresh.fresh.adapters.FreshCategoryFragmentsAdapter;
import com.sabkuchfresh.fresh.models.ProductsResponse;
import com.sabkuchfresh.retrofit.RestClient;
import com.sabkuchfresh.utils.ASSL;
import com.sabkuchfresh.utils.AppStatus;
import com.sabkuchfresh.utils.DialogPopup;
import com.sabkuchfresh.utils.FlurryEventLogger;
import com.sabkuchfresh.utils.FlurryEventNames;
import com.sabkuchfresh.utils.Log;
import com.sabkuchfresh.utils.NudgeClient;
import com.sabkuchfresh.utils.Utils;
import com.sabkuchfresh.widgets.PagerSlidingTabStrip;

import org.json.JSONObject;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class FreshFragment extends Fragment {

	private final String TAG = FreshFragment.class.getSimpleName();
	private LinearLayout linearLayoutRoot;

	private PagerSlidingTabStrip tabs;
	private ViewPager viewPager;
	private FreshCategoryFragmentsAdapter freshCategoryFragmentsAdapter;

	private View rootView;
    private FreshActivity activity;

	public FreshFragment(){}

    @Override
    public void onStart() {
        super.onStart();
        FlurryAgent.init(activity, Config.getFlurryKey());
        FlurryAgent.onStartSession(activity, Config.getFlurryKey());
        FlurryAgent.onEvent(FreshFragment.class.getSimpleName() + " started");
    }

    @Override
    public void onStop() {
		super.onStop();
        FlurryAgent.onEndSession(activity);
    }
	

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_fresh, container, false);


        activity = (FreshActivity) getActivity();
		activity.fragmentUISetup(this);

		linearLayoutRoot = (LinearLayout) rootView.findViewById(R.id.linearLayoutRoot);
		try {
			if(linearLayoutRoot != null) {
				new ASSL(activity, linearLayoutRoot, 1134, 720, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		viewPager = (ViewPager) rootView.findViewById(R.id.viewPager);
		freshCategoryFragmentsAdapter = new FreshCategoryFragmentsAdapter(activity, getChildFragmentManager());
		viewPager.setAdapter(freshCategoryFragmentsAdapter);

		tabs = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
		tabs.setTextColorResource(R.color.theme_color, R.color.grey_dark);
		tabs.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

		viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

			}

			@Override
			public void onPageSelected(int position) {
				try {
					if(activity.getProductsResponse() != null
							&& activity.getProductsResponse().getCategories() != null){
						NudgeClient.trackEventUserId(activity,
								String.format(FlurryEventNames.NUDGE_FRESH_CATEGORY_CLICKED_FORMAT,
								activity.getProductsResponse().getCategories().get(position).getCategoryName()), null);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onPageScrollStateChanged(int state) {

			}
		});

		getAllProducts();

		FlurryEventLogger.event(activity, FlurryEventNames.FRESH_MAIN);

		return rootView;
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if(!hidden){
			freshCategoryFragmentsAdapter.notifyDataSetChanged();
			activity.fragmentUISetup(this);
		}
	}

	public void getAllProducts() {
		try {
			if(AppStatus.getInstance(activity).isOnline(activity)) {

				DialogPopup.showLoadingDialog(activity, activity.getResources().getString(R.string.loading));

				HashMap<String, String> params = new HashMap<>();
				params.put(Constants.KEY_ACCESS_TOKEN, Data.userData.accessToken);
				params.put(Constants.KEY_LATITUDE, String.valueOf(Data.latitude));
				params.put(Constants.KEY_LONGITUDE, String.valueOf(Data.longitude));
				Log.i(TAG, "getAllProducts params=" + params.toString());

				RestClient.getFreshApiService().getAllProducts(params, new Callback<ProductsResponse>() {
					@Override
					public void success(ProductsResponse productsResponse, Response response) {
						String responseStr = new String(((TypedByteArray) response.getBody()).getBytes());
						Log.i(TAG, "getAllProducts response = " + responseStr);
						DialogPopup.dismissLoadingDialog();
						try {
							JSONObject jObj = new JSONObject(responseStr);
							String message = JSONParser.getServerMessage(jObj);
							if (!SplashNewActivity.checkIfTrivialAPIErrors(activity, jObj)) {
								int flag = jObj.getInt(Constants.KEY_FLAG);
								activity.setProductsResponse(productsResponse);
								if(activity.getProductsResponse() != null
										&& activity.getProductsResponse().getCategories() != null) {
									activity.updateCartFromSP();
									activity.updateCartValuesGetTotalPrice();
									freshCategoryFragmentsAdapter.setCategories(activity.getProductsResponse().getCategories());
									tabs.setViewPager(viewPager);
									tabs.setBackgroundColor(activity.getResources().getColor(R.color.white_light_grey));

									if(productsResponse.getShowMessage() != null
											&& productsResponse.getShowMessage().equals(1)) {
										new FreshNoDeliveriesDialog(activity, new FreshNoDeliveriesDialog.Callback() {
											@Override
											public void onDismiss() {

											}
										}).show(message);
									}
								}
							}
						} catch (Exception exception) {
							exception.printStackTrace();
							retryDialog(DialogErrorType.SERVER_ERROR);
						}
						DialogPopup.dismissLoadingDialog();
					}

					@Override
					public void failure(RetrofitError error) {
						Log.e(TAG, "paytmAuthenticateRecharge error" + error.toString());
						DialogPopup.dismissLoadingDialog();
						retryDialog(DialogErrorType.CONNECTION_LOST);
					}
				});
			}
			else {
				retryDialog(DialogErrorType.NO_NET);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void retryDialog(DialogErrorType dialogErrorType){
		DialogPopup.dialogNoInternet(activity,
				dialogErrorType,
				new Utils.AlertCallBackWithButtonsInterface() {
					@Override
					public void positiveClick(View view) {
						getAllProducts();
					}

					@Override
					public void neutralClick(View view) {

					}

					@Override
					public void negativeClick(View view) {
					}
				});
	}


    @Override
	public void onDestroy() {
		super.onDestroy();
        ASSL.closeActivity(linearLayoutRoot);
        System.gc();
	}


}
